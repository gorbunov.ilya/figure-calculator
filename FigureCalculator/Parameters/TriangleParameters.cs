﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureCalculator.Parameters
{
    public class TriangleParameters
    {
        public double A { get; set; }
        public double B { get; set; }
        public double C { get; set; }
        public TriangleParameters(int v1, int v2, int v3)
        {
            A = v1;
            B = v2;
            C = v3;
        }
    }
}
