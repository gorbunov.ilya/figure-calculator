﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureCalculator.Figures
{
    public class UnknownFigure : Figure
    {
        public UnknownFigure(IDictionary<string, object> parameters) : base(parameters)
        {
        }

        protected override double Calculate(IDictionary<string, object> param)
        {
            ValidateRequiredParameters("Func");

            if (param["Func"] is Func<IDictionary<string, object>, double> func)
            {
                return func(param);
            }

            throw new InvalidOperationException($"parameters is wrong");
        }
    }
}
