﻿using System;
using System.Collections.Generic;
using System.Text;
using FigureCalculator.Parameters;

namespace FigureCalculator.Figures
{
    public class Triangle : Figure
    {
        private readonly IDictionary<string, object> _parameters;
        public bool IsRight
        {

            get
            {
                ValidateRequiredParameters("Triangle");

                var parameter = GetTriangleParameter(_parameters["Triangle"]);
                var a = parameter.A;
                var b = parameter.B;
                var c = parameter.C;
                return IsPythagorasTriangle(a, b, c) || IsPythagorasTriangle(a, c, b) || IsPythagorasTriangle(c, b, a);

                bool IsPythagorasTriangle(double leg1, double leg2, double hypotenuse)
                {
                    return Math.Pow(leg1, 2) + Math.Pow(leg2, 2) == Math.Pow(hypotenuse, 2);
                }
            }
        }
        public Triangle(IDictionary<string, object> parameters) : base(parameters)
        {
            _parameters = parameters ?? throw new ArgumentNullException(nameof(parameters));
        }

        protected override double Calculate(IDictionary<string, object> param)
        {
            ValidateRequiredParameters("Triangle");

            var parameter = GetTriangleParameter(param["Triangle"]);
            var p = 0.5 * (parameter.A + parameter.B + parameter.C);
            return Math.Sqrt(p * (p - parameter.A) * (p - parameter.B) * (p - parameter.C));
        }
        private TriangleParameters GetTriangleParameter(object parameter)
        {
            if (parameter is TriangleParameters param)
                return param;

            throw new InvalidOperationException($"parameters {nameof(TriangleParameters)} is wrong");
        }

    }
}
