﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureCalculator.Figures
{
    public class Rectangle : Figure
    {
        public Rectangle(IDictionary<string, object> parameters) : base(parameters)
        {
        }
        protected override double Calculate(IDictionary<string, object> param)
        {
            ValidateRequiredParameters("Length", "Width");

            if (param["Length"] is double length && param["Width"] is double width)
            {
                return length * width;
            }
            throw new InvalidOperationException($"some type is not valid");
        }

        
    }
}
