﻿using System;
using System.Collections.Generic;
using System.Text;

namespace FigureCalculator.Figures
{
    public class Circle : Figure
    {
        public Circle(IDictionary<string, object> parameters) : base(parameters)
        {
        }

        protected override double Calculate(IDictionary<string, object> param)
        {
            ValidateRequiredParameters("Radius");

            if (param["Radius"] is double radius)
            {
                return Math.PI * Math.Pow(radius, 2);
            }

            throw new InvalidOperationException($"parameter Radius is wrong");
        }
    }
}
